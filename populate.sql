-- manufacturers
INSERT INTO manufacturer (name, homepage, search) VALUES ("Espressif", "https://www.espressif.com/", "https://products.espressif.com/#/product-selector");

-- distributors
INSERT INTO distributor (name, homepage, product_page) VALUES ("LCSC", "https://www.lcsc.com/", "https://www.lcsc.com/product-detail/%s.html");
INSERT INTO distributor (name, homepage, product_page) VALUES ("JLCPCB", "https://jlcpcb.com/", "https://jlcpcb.com/partdetail/part/%s");
INSERT INTO distributor (name, homepage, product_page) VALUES ("DigiKey", "https://www.digikey.com/en", "https://www.digikey.com/products/en?keywords=%s");
INSERT INTO distributor (name, homepage, product_page) VALUES ("Mouser", "https://eu.mouser.com/", "https://eu.mouser.com/Search/Refine?Keyword=%s");
INSERT INTO distributor (name, homepage, product_page) VALUES ("Farnell", "https://de.farnell.com/", "https://de.farnell.com/manuf/part/cat/dp/%s");
INSERT INTO distributor (name, homepage, product_page) VALUES ("Octopart", "https://octopart.com/", "https://octopart.com/%s");
INSERT INTO distributor (name, homepage, product_page) VALUES ("AliExpress", "https://www.aliexpress.com/", "https://aliexpress.com/item/%s.html");

-- parts
INSERT INTO part (name, description, manufacturer, datasheet, package, page) VALUES ("ESP32-S3-WROOM-1", "ESP32-S3 module, PCB antenna", (SELECT id FROM manufacturer WHERE name = "Espressif"), "https://www.espressif.com/sites/default/files/documentation/esp32-s3-wroom-1_wroom-1u_datasheet_en.pdf", "module-49", "https://www.espressif.com/en/module/esp32-s3-wroom-1-en");
INSERT INTO part (name, description) VALUES ("ESP32-S3-WROOM-1-N4", "ESP32-S3 module, PCB antenna, 4MB flash");
UPDATE part SET family=(SELECT id FROM part WHERE name = "ESP32-S3-WROOM-1") WHERE name = "ESP32-S3-WROOM-1-N4";

-- distribution
INSERT INTO distribution (part, distributor, sku) VALUES ((SELECT id FROM part WHERE name = "ESP32-S3-WROOM-1-N4"), (SELECT id FROM distributor WHERE name = "LCSC"), "C2913197");
INSERT INTO distribution (part, distributor, sku) VALUES ((SELECT id FROM part WHERE name = "ESP32-S3-WROOM-1-N4"), (SELECT id FROM distributor WHERE name = "Mouser"), "356-ESP32-S3WROOM1N4");

-- locations
INSERT INTO location (name, container) VALUES ("MCU-ice", "ice");
INSERT INTO location (name, container) VALUES ("RF-ice", "ice");

-- stock
INSERT INTO inventory (part, location, quantity) VALUES ((SELECT id FROM part WHERE name = "ESP32-S3-WROOM-1-N4"), (SELECT id FROM location WHERE name = "MCU-ice"), 1);
INSERT INTO inventory (part, location, quantity) VALUES ((SELECT id FROM part WHERE name = "ESP32-S3-WROOM-1-N4"), (SELECT id FROM location WHERE name = "RF-ice"), 2);

-- create some properties
INSERT INTO property (name) VALUES ("category");
INSERT INTO property (name) VALUES ("qeda_path");
INSERT INTO property (name) VALUES ("qeda_variant");
INSERT INTO property (name) VALUES ("kicad_symbol");
INSERT INTO property (name) VALUES ("kicad_footprint");
INSERT INTO property (name) VALUES ("JLCPCB_CORRECTION");

-- set properties
INSERT INTO properties (part, property, value) VALUES ((SELECT id FROM part WHERE name = "ESP32-S3-WROOM-1"), (SELECT id FROM property WHERE name = "category"), "MCU");
INSERT INTO properties (part, property, value) VALUES ((SELECT id FROM part WHERE name = "ESP32-S3-WROOM-1"), (SELECT id FROM property WHERE name = "category"), "RF");
INSERT INTO properties (part, property, value) VALUES ((SELECT id FROM part WHERE name = "ESP32-S3-WROOM-1"), (SELECT id FROM property WHERE name = "category"), "WiFi");
INSERT INTO properties (part, property, value) VALUES ((SELECT id FROM part WHERE name = "ESP32-S3-WROOM-1"), (SELECT id FROM property WHERE name = "category"), "Bluetooth");
