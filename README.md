partdb is an electronic parts inventory manager.

usage
=====

Once the service is installed, there is a single page to search, add, delete, or update parts.
The only aspect not handled by the web frontend is the attachment deletion.
Just remove the files directly on the server under `public/attachments/<part_name>/`.

To add a part, start with the fresh home page without element selected, file the fields (at least the name), and click the update/add button.
You can then find it using the search box.
To import an LCSC part, simply go to the `/import/lcsc/Cxxxx` page and the part will be added to the database.

goals
=====

I wished for a part manager with the following properties:

- self hosted: I do not wish to rely on an external service (which could become unreachable or simply disappear)
- web interface: this removes the need of yet another window running while designing boards, and just integrates well with the remaining part searching activity (e.g. on manufacturer and distributor web pages)
- customizable: simply because everyone has a different designing work flow, and wants to optimize their tools according to it (particularly myself)

One matching solution I found is [PartKeepr](https://partkeepr.org/).
I used it for several year, and it could still be OK for my needs.
But some aspects are just not that optimal anymore:

- it is not developed anymore since 2018 (a docker image keeps it runnable)
- logging in, and searching for parts is slow
- customizing it is really hard (views, features, ...)
- you always have to edit the part and click several times to copy any kind of value

I recently also discovered [Binner](https://binner.io/).
It does not allow to add custom part properties, or third parties distributors.
But it's a young project and seems promising.

Because they did not fit by power user demands, I decided to implement my own solution.
partdb is very crude, but it works, particularly for me.

components
==========

Part information is:

- stored in a sqlite3 database
- retrieved using a sinatra HTTP/REST server
- displayed using minimal HTML+JS pages

installation
============

install software dependencies (here for Arch linux):

~~~
sudo pacman -S sqlite ruby ruby-rake
gem install json sqlite3 sinatra puma
~~~

create database

~~~
sqlite partdb.db < schema.sql
~~~

now just run `server.rb`.
now go to http://localhost:4245 and you are ready to use it.
the port and database file is defined in `server.rb`.

use a proxy web server to handle the authentication, TLS, compression, ...

KiCAD
=====

KiCAD can have a [KiCad database library](https://docs.kicad.org/master/en/eeschema/eeschema.html#database_libraries).
Running the `kicad_lib.rb` script will create tables (actually views) for each partdb category to be used by KiCAD.
The views are added without the `partdb.db` database file itself.
The script will also create the `partdb.kicad_dbl` library file.
This is the file to be used by KiCAD to add the database library.
But first you also need to install and configure ODBC so KiCAD can actually access the database:

~~~
# install ODBC
sudo pacman -S unixodbc
# install OBDC sqlite driver
pikaur -S sqliteodbc
# fix ODBC sqlite configuration
sudo sed -i 's|/usr/lib64/libsqlite3odbc.so|/usr/lib/libsqlite3odbc.so|g' /etc/odbcinst.ini
sudo sed -i 's|/usrl/lib64/libsqlite3odbc.so|/usr/lib/libsqlite3odbc.so|g' /etc/odbcinst.ini
sudo sed -i 's|/usr/lib64/libsqliteodbc.so|/usr/lib/libsqlite3odbc.so|g' /etc/odbcinst.ini
sudo sed -i 's|/usrl/lib64/libsqliteodbc.so|/usr/lib/libsqlite3odbc.so|g' /etc/odbcinst.ini
# add database
cat << EOF | sudo tee -a /etc/odbc.ini
[partdb]
Description=electronic parts database
Driver=SQLite
Database=<path to partdb.db>
Timeout=2000
EOF
# ensure it works
isql partdb
SELECT * FROM part LIMIT 1;
exit
~~~

service
=======

For convenience you can install the systemD service.
Be sure to adjust the path to the partdb script.

~~~
mkdir -p ~/.local/share/systemd/user/
cp partdb.service ~/.local/share/systemd/user/
systemctl --user start partdb.service
~~~
